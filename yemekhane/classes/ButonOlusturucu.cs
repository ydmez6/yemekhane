﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace yemekhane.classes
{
    public class myButton
    {
        public string txt;
        public Brush background;
        public PackIconKind pic;
        public MethodInfo reh;
        public double width;
        public double height = 80;
        public double margin = 10;
        public int x;
        public int y;
        internal Brush foreground;
        internal MethodInfo sub_title;
    }
}
