﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yemekhane.classes;


namespace yemekhane.classes.WebServis
{
    class WGenel
    {
       
        public string YanitMesaj { get; set; }
        public string Token { get; set; }
        rfGenel.GenelSoapClient Genel = new rfGenel.GenelSoapClient();
        public List<string> GirisYap(string KullaniciAdi,string Sifre)
        {
            DataSet Ds= Genel.KullaniciGiris(KullaniciAdi,Sifre);
            DataTable yanit = Ds.Tables[0];
            List<string> yetkilist = new List<string>();

            foreach (DataRow item in yanit.Rows)
            {
                YanitMesaj = item["mesaj"].ToString();
                Token = item["Token"].ToString();
            }
            if (Ds.Tables.Count==1)
            {
                return yetkilist;
            }
          
            
           
            DataTable yetkiler = Ds.Tables[1];
            foreach (DataRow item in yetkiler.Rows)
            {
                yetkilist.Add(item["Yetki"].ToString());
            }
            return yetkilist;
        }
        public DataTable MenuCek()
        {
            DataSet Ds = Genel.MenuGetir();
            DataTable yanit = Ds.Tables[0];
            foreach (DataRow item in yanit.Rows)
            {
                YanitMesaj = item["mesaj"].ToString();
                Token = item["Token"].ToString();
            }
            if (Ds.Tables.Count==1)
            {
                return new DataTable();
            }
            return Ds.Tables[1];
        }
    }
}
