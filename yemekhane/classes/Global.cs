﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yemekhane.classes;

namespace yemekhane.classes
{
   public static class Global
    {
        public static string KullaniciAdi { get; set; }
        public static List<string> Yetkiler { get; set; }
        public static string Token { get { return _Token; }  set { _Token = aes.Sifrele(value, "FSC38BBIFJ33"); } }
        static string _Token;
        public static string Dil { get; set; }
     

        public static bool YetkiKontrol(string Yetki)
        {
            foreach (var item in Yetkiler)
            {
                if (item==Yetki)
                {
                    return true;
                }
                
            }
            return false;
        }
    }
}
