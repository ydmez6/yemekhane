﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using yemekhane.classes;
using yemekhane.classes.WebServis;
using yemekhane.anaformlar;

namespace yemekhane
{
    /// <summary>
    /// MainWindow.xaml etkileşim mantığı
    /// </summary>
    public partial class MainWindow : Window
    {
        List<k_Menu> menuler;
        public MainWindow()
        {
            InitializeComponent();
            menuler = MenuGetir();
            loadButtons();
        }
        WGenel Genel = new WGenel();
        List<k_Menu> MenuGetir()
        {
            List<k_Menu> _menuler = new List<k_Menu>();
            DataTable dt = Genel.MenuCek();
            foreach (DataRow item in dt.Rows)
            {
                _menuler.Add(new k_Menu
                {
                    aktif = byte.Parse(item["aktif"].ToString()),
                    background = item["background"].ToString(),
                    col = int.Parse(item["col"].ToString()) ,
                    foreground = item["foreground"].ToString(),
                    function = item["function"].ToString(),
                    id = int.Parse(item["id"].ToString()),
                    name = item["name"].ToString(),
                    pic = int.Parse(item["pic"].ToString()),
                    row = int.Parse(item["row"].ToString()),
                    sira = int.Parse(item["sira"].ToString()),
                    sub_function = item["sub_function"].ToString(),
                    ust_menu = int.Parse(item["ust_menu"].ToString()),
                    yetki = item["yetki"].ToString()

                });
            }
            return _menuler;
        }
        int xd = 0;
        public string deneme_txt()
        {
            return "";
        }
        #region Dinamik buton eventlar
        public void KullanicilarClick(object sender, RoutedEventArgs e)
        {
            Window w =new K_Kullanici();
            w.ShowDialog();
        }

        #endregion

        #region Buton ve menü oluşturma
        public void bosmetot(object sender, RoutedEventArgs e)
        {
           
        }

        public void loadButtons()
        {
            xd++;
            gridMain.Children.OfType<Grid>().Where(a => a.Tag != null && a.Tag.ToString() == "buts").ToList().ForEach(a =>
            {

                gridMain.Children.Remove(a);
                gridMain.UnregisterName(a.Name);
                a = null;
            });


            List<k_Menu> _ustmenuler = new List<k_Menu>();
            foreach (var item in menuler)
            {
                if (item.ust_menu==0)
                {
                    _ustmenuler.Add(item);
                }
            }
            foreach (var ust_menu in _ustmenuler)
            {
                List<k_Menu> alt_menus = new List<k_Menu>();
                foreach (var item in menuler)
                {
                    if (item.ust_menu==ust_menu.id)
                    {
                        alt_menus.Add(item);
                    }
                }

                List<myButton> alt_menuler = new List<myButton>();
                foreach (var alt_menu in alt_menus)
                {
                    bool yetkivar = false;
                    foreach (var item in Global.Yetkiler)
                    {
                        if (alt_menu.yetki==item)
                        {
                            yetkivar = true;
                        }
                    }
                    if (yetkivar == false)
                        continue;
                    Type thisType = this.GetType();
                    string metot = "";
                    if (alt_menu.function=="-1")
                    {
                        metot = "bosmetot";
                    }
                    else
                    {
                       metot= alt_menu.function;
                    }
                    MethodInfo theMethod = thisType.GetMethod(metot);

                    MethodInfo theTitle = thisType.GetMethod("deneme_txt");
                    if (alt_menu.sub_function != null)
                    {
                        theTitle = thisType.GetMethod(alt_menu.sub_function);

                        if (theTitle == null)
                        {
                            theTitle = thisType.GetMethod("deneme_txt");
                        }
                    }

                    if (theMethod == null)
                    {
                        theMethod = thisType.GetMethod("deneme");
                    }

                    //theMethod.Invoke(this, userParameters);
                    PackIconKind ikon = new PackIconKind();
                    if (alt_menu.pic == -1)
                    {
                        alt_menuler.Add(new myButton()
                        {
                            background = new SolidColorBrush((Color)FindResource(alt_menu.background)),
                            foreground = new SolidColorBrush((Color)FindResource(alt_menu.foreground)),
                            txt = alt_menu.name,
                            reh = theMethod,
                            sub_title = theTitle
                        });

                    }
                    else
                    {
                        ikon = (PackIconKind)alt_menu.pic;
                        alt_menuler.Add(new myButton()
                        {
                            background = new SolidColorBrush((Color)FindResource(alt_menu.background)),
                            foreground = new SolidColorBrush((Color)FindResource(alt_menu.foreground)),
                            txt = alt_menu.name,
                            pic = ikon,
                            reh = theMethod,
                            sub_title = theTitle
                        });

                    }


                }
                foreach (var item in Global.Yetkiler)
                {
                    if (ust_menu.yetki==item)
                    {
                        
                        if (ust_menu.col == -1)
                            ust_menu.col = 0;
                        if (ust_menu.row == -1)
                            ust_menu.row = 0;
                       
                        createMenu(ust_menu.col.GetValueOrDefault(), ust_menu.row.GetValueOrDefault(), alt_menuler, ust_menu.name);

                    }
                }
            }

        }
        public void createMenu(int col, int row, List<myButton> buts, string title)
        {
            //controller.lblLog = lblStatus;

            Grid DynamicGrid = new Grid();
            DynamicGrid.VerticalAlignment = VerticalAlignment.Stretch;
            DynamicGrid.ShowGridLines = true;
            DynamicGrid.Name = "buttonGrid" + col.ToString() + "_" + row.ToString() + "x" + xd.ToString();


            DynamicGrid.Tag = "buts";
            DynamicGrid.HorizontalAlignment = HorizontalAlignment.Stretch;
            Panel.SetZIndex(DynamicGrid, 2);

            Grid.SetColumn(DynamicGrid, col);
            Grid.SetRow(DynamicGrid, row);


            gridMain.RegisterName(DynamicGrid.Name, DynamicGrid);
            gridMain.Children.Add(DynamicGrid);
            InitializeComponent();

            int nums = 5;
            if (col == 0)
            {
                nums = 1;
                Grid.SetRowSpan(DynamicGrid, 2);
            }
            else
            {
                Grid.SetColumnSpan(DynamicGrid, 2);
            }

            gridCreate(buts, DynamicGrid, nums, title);

        }
        void gridCreate(List<myButton> btnlist, Grid grid, int col, string title)
        {

            TextBlock tb = new TextBlock() { Text = title, FontSize = 20, Height = 30 };
            tb.Margin = new Thickness(0, 10, 0, 0);
            tb.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#2196F3"));
            tb.HorizontalAlignment = HorizontalAlignment.Left;
            tb.VerticalAlignment = VerticalAlignment.Top;
            grid.Children.Add(tb);
            Grid.SetColumn(tb, 0);
            Grid.SetRow(tb, 0);

            Grid button_container = new Grid();

            button_container.HorizontalAlignment = HorizontalAlignment.Stretch;
            int i = 0;
            for (i = 0; i < col; i++)
            {
                button_container.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            }

            for (i = 0; i < Math.Ceiling((double)btnlist.Count / col); i++)
            {
                button_container.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            }

            i = 0;
            foreach (myButton _btn in btnlist)
            {
                _btn.x = i % col;
                _btn.y = Convert.ToInt32(i / col);

                //_btn.width = grid.ActualWidth / col;
                if (_btn.pic == null)
                {

                }
                Button btn = gridButton(_btn, tb.Height);
                btn.Name = "dynamicButs" + _btn.x + "_" + _btn.y;
                btn.Tag = "dynamicButs";

                button_container.Children.Add(btn);


                Binding asd = new Binding();
                // { Binding ActualHeight, ElementName = MyControlNameFromCell1, Mode = OneWay}
                asd.Source = button_container.RowDefinitions[0].Height;
                asd.Mode = BindingMode.OneWay;
                btn.SetBinding(Button.HeightProperty, asd);

                Grid.SetColumn(btn, _btn.x);
                Grid.SetRow(btn, _btn.y);

                i++;
            }
            button_container.Margin = new Thickness(0, tb.Height + 5, 0, 15);

            grid.Children.Add(button_container);
        }
        public Button gridButton(myButton _btn, double txt_offset)
        {

            DockPanel sp = new DockPanel();
            sp.HorizontalAlignment = HorizontalAlignment.Stretch;

            TextBlock tb = new TextBlock() { Text = _btn.txt };
            tb.Margin = new Thickness(10, 0, 0, 0);
            tb.Foreground = new SolidColorBrush(Color.FromScRgb(100, 255, 255, 255));
            tb.Opacity = 0.7;
            tb.FontSize = 20;
            tb.HorizontalAlignment = HorizontalAlignment.Left;
            tb.VerticalAlignment = VerticalAlignment.Top;

            TextBlock tb_sub = new TextBlock() { Text = _btn.sub_title.Invoke(this, new object[] { }).ToString() };
            tb_sub.Margin = new Thickness(10, 0, 0, 60);
            tb_sub.Foreground = new SolidColorBrush(Color.FromScRgb(100, 255, 255, 255));
            tb_sub.Opacity = 0.7;
            tb_sub.FontSize = 15;
            tb_sub.HorizontalAlignment = HorizontalAlignment.Right;
            tb_sub.VerticalAlignment = VerticalAlignment.Top;

            DockPanel.SetDock(tb, Dock.Top);


            PackIcon pi = new PackIcon() { Kind = _btn.pic };
            //pi.Margin = new Thickness((_btn.width - _btn.margin * 3) / 2, (_btn.height - _btn.margin * 3) / 3, 0, 0);
            pi.Margin = new Thickness(0, 0, 0, 10);
            pi.Foreground = new SolidColorBrush(Color.FromScRgb(100, 255, 255, 255));
            pi.Opacity = 0.7;
            pi.Height = pi.Width = 75;
            //pi.Height = _btn.height / 2;
            Panel.SetZIndex(pi, 1);

            DockPanel.SetDock(pi, Dock.Bottom);

            pi.HorizontalAlignment = HorizontalAlignment.Right;
            pi.VerticalAlignment = VerticalAlignment.Bottom;

            sp.Children.Add(tb);
            sp.Children.Add(tb_sub);

            if (_btn.pic.ToString() != "AccessPoint")
            {
                sp.Children.Add(pi);
            }
            Button btn = new Button();
            //btn.Width = _btn.width - _btn.margin;
            //btn.Height = _btn.height;

            btn.Background = _btn.background;
            //btn.Click += _btn.reh;
            Delegate asd = Delegate.CreateDelegate(Button.ClickEvent.HandlerType, this, _btn.reh, false);
            btn.AddHandler(Button.ClickEvent, asd);

            btn.Foreground = _btn.foreground;
            btn.HorizontalAlignment = HorizontalAlignment.Stretch;
            btn.VerticalAlignment = VerticalAlignment.Stretch;
            btn.Padding = new Thickness(0, 0, 0, 0);
            btn.BorderBrush = (SolidColorBrush)FindResource("MaterialDesignDivider");

            //btn.Margin = new Thickness((_btn.width ) * _btn.x, (_btn.margin + _btn.height) * _btn.y + txt_offset, 0, 0);
            btn.Margin = new Thickness(10, 10, 10, 0);
            btn.Content = sp;
            btn.HorizontalContentAlignment = HorizontalAlignment.Stretch;
            btn.VerticalContentAlignment = VerticalAlignment.Stretch;


            DropShadowEffect dropShadowEffect = new DropShadowEffect();
            dropShadowEffect.Opacity = 0.25;
            dropShadowEffect.ShadowDepth = 10;
            dropShadowEffect.BlurRadius = 10;
            dropShadowEffect.Color = Colors.Black;
            btn.Effect = dropShadowEffect;


            return btn;
        }
#endregion
    }

}
