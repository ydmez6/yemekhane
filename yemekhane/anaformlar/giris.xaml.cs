﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using yemekhane.classes.WebServis;
using yemekhane.classes;
using System.Globalization;
using System.Resources;
using System.Configuration;
using System.Threading;
using yemekhane.Dil;

namespace yemekhane.anaformlar
{
    /// <summary>
    /// giris.xaml etkileşim mantığı
    /// </summary>
    public partial class giris : Window
    {
        void DilDegistir()
        {
            Properties.Resources.Culture = new CultureInfo(Global.Dil);

            Thread.CurrentThread.CurrentCulture = new CultureInfo(Global.Dil);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
           
            lblDil.Content = Dil.dil.GfDilLblTx;
            txtMail.Text = Dil.dil.GfKullaniciTx;
            txtPass.Password = Dil.dil.GfSifreTx;
            btnLogin.Content = Dil.dil.GfGirisBtnTx;
            this.Title = dil.GfBaslik;
            ListBoxItem lsi = new ListBoxItem();
            foreach (ListBoxItem item in cmbDil.Items)
            {
                if (item.Tag.ToString()==Global.Dil)
                {
                    lsi = item;
                }
            }
            cmbDil.SelectedItem = lsi;
           
        }
        public giris()
        {
            Global.Dil = CultureInfo.CurrentCulture.ToString();
            
            InitializeComponent();
           
          
            cmbDil .Items.Add(new ListBoxItem
            {
                Content = "English",
                Tag = "en-US"
            });
            cmbDil.Items.Add(new ListBoxItem
            {
                Content = "Türkçe",
                Tag = "tr-TR"
            });
            DilDegistir();
        }
        

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            WGenel wg = new WGenel();
            
          List<string> yetkiler=  wg.GirisYap(txtMail.Text, txtPass.Password);
            
            if (wg.YanitMesaj=="1")
            {
                Global.Yetkiler = yetkiler;
                Global.Token = wg.Token;
                Window mw = new MainWindow();
                mw.Show();
                this.Close();
            }
            else if (wg.YanitMesaj=="0")
            {
                MessageBox.Show(dil.GfLoginBasarisizMsg, dil.GfLoginBasarisizMsgBaslik, MessageBoxButton.OK, MessageBoxImage.Stop);
            }
            else
            {
                MessageBox.Show(dil.GfLoginHataMsg, dil.GfLoginHataMsgBaslik, MessageBoxButton.OK, MessageBoxImage.Error);

            }
        }

        private void txtMail_GotFocus(object sender, RoutedEventArgs e)
        {
            if (txtMail.Text == dil.GfKullaniciTx)
            {
                txtMail.Text = "";
            }
        }

        private void txtMail_LostFocus(object sender, RoutedEventArgs e)
        {
            if (txtMail.Text == "")
            {
                txtMail.Text = dil.GfKullaniciTx;
            }
           
        }

        private void txtPass_LostFocus(object sender, RoutedEventArgs e)
        {
            if (txtPass.Password == "")
            {
                txtPass.Password = dil.GfSifreTx;
            }
        }

        private void txtPass_GotFocus(object sender, RoutedEventArgs e)
        {
            if (txtPass.Password == dil.GfSifreTx)
            {
                txtPass.Password = "";
            }
        }

        private void cmbDil_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            Global.Dil = ((ListBoxItem)cmbDil.SelectedItem).Tag.ToString();
            DilDegistir();

        }
    }
}
