﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using yemekhane.Dil;
using yemekhane.classes;

namespace yemekhane.anaformlar
{
    /// <summary>
    /// K_Kullanici.xaml etkileşim mantığı
    /// </summary>
    public partial class K_Kullanici : Window
    {
        void Dildegistir()
        {
            Properties.Resources.Culture = new CultureInfo(Global.Dil);

            Thread.CurrentThread.CurrentCulture = new CultureInfo(Global.Dil);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
            btnKullaniciDuzenle.Content = dil.KkKullaniciDuzenleBtnTx;
            btnKullaniciEkle.Content = dil.KkKullaniciEkleBtnTx;
            btnKullaniciSil.Content = dil.KkKullaniciSilBtnTx;
            lblKullaniciAd.Content = dil.KkKullaniciAraLblTx;
            this.Title = dil.KkBaslik;
        }
        public K_Kullanici()
        {
            
            InitializeComponent();
            Dildegistir();
        }
    }
}
