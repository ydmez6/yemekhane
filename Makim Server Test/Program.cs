﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Makim_Server_Test
{

    class Program
    {
        public static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
       static string ServerIp=ConfigurationManager.AppSettings["ServerIp"].ToString();
       static string ServerPort=ConfigurationManager.AppSettings["ServerPort"].ToString();
        static void Main(string[] args)
        {
           
            Console.Clear();
            //log.Debug("sdsd");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("------Makim test sunucusu başlatıldı------");
           
            Server server = new Server();
            server.OnDisconnectedEvent += OnDisconnectedEvent;
            IPAddress ip = IPAddress.Parse("192.168.0.221");
            server.Bind(new IPEndPoint(IPAddress.Parse(ServerIp),int.Parse(ServerPort))); //Seninle aynı portu kullandım. :) (Server üzerinde çalışırken Loopback'i Any yap. Server.cs içinde Bind metoduna gerekli açıklamayı yaptım.)
            server.Listen(-1);
            server.Start();
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine(string.Format("Server başlatıldı : {0}:{1}",ServerIp,ServerPort));
            Console.ResetColor();
            Thread ServerDurum = new Thread(ServerDurumu);
            ServerDurum.IsBackground = true;
            ServerDurum.Start();
            Thread.Sleep(-1);
        }
       static void ServerDurumu()
        {
            while (true)
            {
                Console.WriteLine("Bağlı istemci sayısı : " + Isleyici.Cihazlar.Count);
              
                Thread.Sleep(5000);
                foreach (var item in Isleyici.Cihazlar)
                {
                    item.Disconnect();
                }
            }
         
        }
        static void OnDisconnectedEvent(IPEndPoint endPoint)
        {
            string ipadres = endPoint.ToString();
            ipadres = ipadres = ipadres.Substring(0, ipadres.IndexOf(":"));
           
            Console.WriteLine(endPoint.ToString() + " cihaz bağlantısı kesildi");
        }
    }
}
