﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Makim_Server_Test
{
    public delegate void SocketDisconnectedDelegate(IPEndPoint endPoint); //Disconnect için event oluşturabilmek adına bu delegate tanımlandı.
    class Server
    {

        private Socket _listener;
        private bool _listening;

        public event SocketDisconnectedDelegate OnDisconnectedEvent; //Disconnect event.
        public Server()
        {
            this._listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
          
        }

       
        public void Bind(IPEndPoint localEP)
        {
            this._listener.Bind(localEP);

        }
       
        public void Listen(int backlog)
        {
            this._listener.Listen(backlog);
            this._listening = true;
        }
        
        public void Start()
        {
           
            new Thread(new ThreadStart(() =>
            {
                while (this._listening)
                {
                    Socket accepted = this._listener.Accept();
                    Cihaz Cihaz = new Dogrulama().CihazDogrula(accepted);
                    Cihaz.OnDisconnectedEvent += Disconnect;
                    if (Cihaz.Dogrulandi == true)
                    {
                        Isleyici.Cihazlar.Add(Cihaz);
                        Console.WriteLine("Yeni cihaz bağlandı | Cihaz tipi : " + Cihaz.Tip + " | Ip adresi: " + Cihaz.Ip + " | Bağlantı zamanı : " + DateTime.Now.ToString());
                        Cihaz.VeriAkisi();
                    }
                    else
                    {
                        accepted.Close();
                    }


                }
            }))
            { IsBackground = true }.Start();


          }
        static void Disconnect(IPEndPoint endPoint)
        {
            string ipadres = endPoint.ToString();
            ipadres = ipadres = ipadres.Substring(0, ipadres.IndexOf(":"));
           
            Console.WriteLine(endPoint.ToString() + " cihaz bağlantısı kesildi");
        }
    }
}
