﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Makim_Server_Test
{
    static class Converter
    {
        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Local);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }
        public static long DateTimeToUnixTimeStamp(DateTime DateTime)
        {
            long epoch = (DateTime.Ticks - 621355968000000000) / 10000000;



            return epoch;
        }
        public static string Cmrkartduzelt(string card)
        {
            string orjinal = "";
            try
            {
                orjinal = int.Parse(card).ToString("X");
            }
            catch (Exception)
            {
                orjinal = card;
            }
            string cardsonuc = "";
            try
            {
                for (int i = 7; i > 0; i -= 2)
                {
                    cardsonuc += orjinal[i - 1].ToString() + orjinal[i].ToString();
                }
                return cardsonuc;
            }
            catch (Exception)
            {
                return card;
            }
        }
    }
}
