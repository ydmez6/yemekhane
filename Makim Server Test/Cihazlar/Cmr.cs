﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Makim_Server_Test.Cihazlar
{
    public class CmrGiden :CihazGiden
    {
        public int Role1Sure { get; set; }
        public int BuzzerSure { get; set; }
        public int BuzzerTekrar { get; set; }
        public string LcdYaziBuyuk { get; set; }
        public string LcdYaziKucuk { get; set; }
        public long Zaman { get; set; }

       
    }
    public class CmrGelen :CihazGelen
    {
        public string CihazId { get; set; }
        public string Stat { get; set; }
        public string Kart { get; set; }
        public long Zaman { get; set; }
    }
   public class Cmr
    {

        string _CihazAd;
        string _CihazId;
        string _Mac;
        IPAddress _Ip;
        Socket _Socket;
        public string CihazId { get {return _CihazId; } }
        public string CihazAd { get { return _CihazAd; } }
        public string Mac { get { return _Mac; } }
        public string Ip { get { return _Ip.ToString(); } }
        public bool Baglimi { get { return _Socket.Connected; } }
        public  Cmr(string _cihaz_ad,string _cihaz_id,Socket _Socket)
        {
            this._CihazAd = _cihaz_ad;
            this._CihazId = _cihaz_id;
            this._Socket = _Socket;
        
        }
        public void Disconnect()
        {
            if (_Socket.Connected)
                _Socket.Close();
        }

        public bool VeriYolla(CmrGiden Veri)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(_CihazId.ToString());
                if (Veri.Role1Sure != 0)
                    sb.Append(Veri.Role1Sure.ToString());
                if (Veri.BuzzerSure != 0)
                    sb.Append(Veri.BuzzerSure.ToString());
                if (Veri.BuzzerTekrar != 0)
                    sb.Append(Veri.BuzzerTekrar.ToString());
                if (!string.IsNullOrEmpty(Veri.LcdYaziBuyuk))
                    sb.Append(Veri.LcdYaziBuyuk);
                if (!string.IsNullOrEmpty(Veri.LcdYaziKucuk))
                    sb.Append(Veri.LcdYaziKucuk);
                if (Veri.Zaman != 0)
                    sb.Append(Veri.Zaman.ToString());
                byte[] buffer = Encoding.ASCII.GetBytes(sb.ToString());

                _Socket.Send(buffer);
                return true;
            }
            catch (Exception)
            {
                return false;
                
            }
            
        }
        public void VeriAkisi(string GelenVeri)
        {
            CmrGelen cm = GeleniAyir(GelenVeri);
            Console.WriteLine(string.Format("Kart numarası: {0} - İşlem zamanı: {1}",int.Parse(Converter.Cmrkartduzelt(cm.Kart), System.Globalization.NumberStyles.HexNumber),Converter.UnixTimeStampToDateTime(cm.Zaman)));
        }
        private CmrGelen GeleniAyir(string Gelen)
        {
            string[] arr = Gelen.Split(',');
            return new CmrGelen { CihazId = arr[0], Stat = arr[1].Remove(0, 5), Kart = arr[2].Remove(0, 4), Zaman = long.Parse(arr[3].Remove(0, 5)) };
           
        }
    }
}
