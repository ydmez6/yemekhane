﻿using Makim_Server_Test.Cihazlar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Makim_Server_Test
{
    public class CihazGiden
    {

    }
    public class CihazGelen
    {

    }
    public class Cihaz
    {
        public static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public event SocketDisconnectedDelegate OnDisconnectedEvent;
        CihazTipi _CihazTipi;
        string _CihazIsmi;
        string _Mac;
        IPAddress _Ip;
        bool _Dogrulandi = false;
        public CihazTipi Tip { get { return _CihazTipi; } }
        public string Ad { get { return _CihazIsmi; } }
        public string Mac { get { return _Mac; } }
        public IPAddress Ip { get { return _Ip; } }
        public bool Dogrulandi { get { return _Dogrulandi; } set { _Dogrulandi = value; } }
        Cmr Cmr;
        Socket _Socket;
        public Cihaz(CihazTipi _CihazTipi, string _CihazAd, string _Cihaz_Id, Socket _Socket)
        {
            this._Socket = _Socket;
             IPEndPoint remoteEndPoint = _Socket.RemoteEndPoint as IPEndPoint;
            string cihazip = remoteEndPoint.ToString();
            cihazip = cihazip.Substring(0, cihazip.IndexOf(":"));
            _Ip = IPAddress.Parse(cihazip);
            if (_CihazTipi == CihazTipi.Cmr)
            {
                this._CihazTipi = CihazTipi.Cmr;
                Cmr = new Cmr(_CihazAd, _Cihaz_Id, _Socket);
            }
               
        }

        public bool VeriYolla(CihazGiden Veri)
        {

            if (_CihazTipi == CihazTipi.Cmr)
                return Cmr.VeriYolla((CmrGiden)Veri);


            return false;
        }
        public void Disconnect()
        {
            if (Cmr.Baglimi)
                Cmr.Disconnect();
        }
        public void VeriAkisi()
        {
            IPEndPoint remoteEndPoint = _Socket.RemoteEndPoint as IPEndPoint;
            new Thread(new ThreadStart(() =>
            {

                while (true)
                {
                    byte[] temp = new byte[555];
                    int read;
                    string result;
                    try
                    {
                        read = _Socket.Receive(temp, 0, 555, SocketFlags.None);
                        byte[] received = new byte[read];
                        Array.Copy(temp, 0, received, 0, read);
                        result = System.Text.Encoding.ASCII.GetString(received);
                        result = result.Substring(0, received.Length);
                        string cihazip = remoteEndPoint.ToString();
                        cihazip = cihazip.Substring(0, cihazip.IndexOf(":"));
                        if (this._CihazTipi == CihazTipi.Cmr)
                            Cmr.VeriAkisi(result);

                        if (read == 0 || read == -1)
                        {
                            _Socket.Close();
                            break;
                        }
                     
                    }
                    catch (SocketException exception)
                    {
                        if (exception.SocketErrorCode == SocketError.ConnectionAborted) //Eğer soket "karşı taraftan" kapatılmışsa, ConnectionAborted döner.
                        {
                            break;
                        }
                        else if (exception.SocketErrorCode == SocketError.TimedOut)
                        {
                            _Socket.ReceiveTimeout = 0; //Bu benim tercihimdir. :)
                            continue;
                        }
                        else if (exception.SocketErrorCode == SocketError.WouldBlock)
                        {
                            _Socket.Blocking = true;
                        }
                        else
                        {
                            _Socket.Close();
                            break;
                        }
                    }
                    catch (ObjectDisposedException)
                    {
                        break;
                    }
                    byte[] buffer = Encoding.ASCII.GetBytes("MCR04L-FF7F150E,ROLE1=5000,BUZZER;1;1,LCDCLR,LCDSET;10;20;2;Test,LCDSET;10;35;1;www.makim.com.tr,TSYNC="+Converter.DateTimeToUnixTimeStamp(DateTime.Now));
                    try
                    {
                        _Socket.Send(buffer);
                    }
                    catch (SocketException)
                    {
                        _Socket.Close();
                        return;
                    }
                    catch (ObjectDisposedException)
                    {
                        break;
                    }
                }
                Console.WriteLine("yok");
                if (this.OnDisconnectedEvent != null) //Bu da kesilen soket bağlantısı için bir event çağırır.
                    this.OnDisconnectedEvent(remoteEndPoint);
            }))
            { IsBackground = true }.Start();
        }
    }
}
