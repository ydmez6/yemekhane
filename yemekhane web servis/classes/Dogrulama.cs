﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace yemekhane_web_servis.classes
{
    public class Dogrulama : System.Web.Services.WebService
    {
        db.veritabaniDataContext db = new yemekhane_web_servis.db.veritabaniDataContext();

        public bool Dogrulandi { get{ return _dogrulandi; }  }
        public string Token { get { return _token; } }
        string _token="";
        bool _dogrulandi=false;
        public string msg = "";
        public  Dogrulama(string token,string yetki)
        {
            if (Session["Kullanici"] == null)
            {
                _dogrulandi = false;
                msg = "kyok";
                return;
            }
         
            
            var ka = (db.k_kullanici)Session["Kullanici"];
            Genelislemler gi = new Genelislemler();
            msg = "yetkiyok";
            if (gi.YetkiKontrol(yetki, ka))
            {
                msg = "yetkivar";
                _token = gi.TokenCek(ka.kullanici_adi).Trim();
                if (_token == aes.SifreyiCoz(token,"FSC38BBIFJ33"))
                {
                    _dogrulandi = true;
                    msg = "tmm";
                }

            }
        }
    }
}