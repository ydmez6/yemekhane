﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace yemekhane_web_servis.classes
{
    public class Genelislemler
    {
        db.veritabaniDataContext db = new yemekhane_web_servis.db.veritabaniDataContext();
        public static String Sifrele(string sifre)
        {
            HashAlgorithm hashAlgorithm = (HashAlgorithm)new SHA1Managed();
            using (hashAlgorithm)
            {
                byte[] bytes = Encoding.Default.GetBytes(sifre);
                return BitConverter.ToString(hashAlgorithm.ComputeHash(bytes));
            }
        }
        public string TokenCek(string kullaniciad)
        {
            var token = db.k_kullanici_token.Where(a => a.aktif == 1 && a.kullanici_ad == kullaniciad).OrderByDescending(x => x.id).FirstOrDefault();
            if (token!=null)
            {
                return token.token;
            }
            else
            {
                return "token yok";
            }
        }
        public string TokenEkle(string kullaniciad)
        {
            string tokenid = Guid.NewGuid().ToString("N");
            int a = tokenid.Length;
            var _token = new db.k_kullanici_token {
                aktif = 1,
                eklemezamani=DateTime.Now,
                kullanici_ad=kullaniciad,
                token=tokenid
                
            };

            db.k_kullanici_token.InsertOnSubmit(_token);
            db.SubmitChanges();
            return tokenid;
        }
        public bool YetkiKontrol(string yetki,db.k_kullanici Kullanici)
        {
            
            db.k_rol_yetki _yetki = db.k_rol_yetki.Where(a => a.aktif == 1 && a.yetki == yetki && a.rol_id == Kullanici.rol_id).FirstOrDefault();
            if (_yetki == null)
                return false;
            return true;
        }
       
    }
}