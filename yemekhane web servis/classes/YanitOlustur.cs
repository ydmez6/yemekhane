﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace yemekhane_web_servis.classes
{
    public class YanitOlustur
    {
        public static DataTable YanitSablon()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("mesaj", typeof(string));
            dt.Columns.Add("token", typeof(string));
            return dt;
        }
    }
}