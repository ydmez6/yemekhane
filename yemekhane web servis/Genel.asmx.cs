﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using yemekhane_web_servis.classes;

namespace yemekhane_web_servis
{
    /// <summary>
    /// Genel için özet açıklama
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Bu Web Hizmeti'nin, ASP.NET AJAX kullanılarak komut dosyasından çağrılmasına, aşağıdaki satırı açıklamadan kaldırmasına olanak vermek için.
    // [System.Web.Script.Services.ScriptService]
    public class Genel : System.Web.Services.WebService
    {
        protected string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
        db.veritabaniDataContext db = new db.veritabaniDataContext();
        [WebMethod(EnableSession = true)]
        public DataSet KullaniciGiris(string KullaniciAd, string Sifre)
        {
            var kullanici = db.k_kullanici.Where(a => a.kullanici_adi == KullaniciAd && a.sifre == Genelislemler.Sifrele(Sifre) && a.aktif == 1).FirstOrDefault();
            if (kullanici == null)
            {

                DataTable dt = YanitOlustur.YanitSablon();
                DataSet ds = new DataSet("yanit");
                dt.Rows.Add("0", "");
                ds.Tables.Add(dt);
                return ds;

            }
            else
            {
               
                Session["Kullanici"] = kullanici;
                DataTable dt = YanitOlustur.YanitSablon();
                DataSet ds = new DataSet("yanit");
                DataTable yt = new DataTable();
                yt.Columns.Add("yetki", typeof(string));
                var kullaniciyetkiler = db.k_rol_yetki.Where(a => a.aktif == 1 && a.rol_id == kullanici.rol_id);
                foreach (var item in kullaniciyetkiler)
                {
                    yt.Rows.Add(item.yetki);
                }
                dt.Rows.Add("1", new Genelislemler().TokenEkle(KullaniciAd));
                ds.Tables.Add(dt);
                ds.Tables.Add(yt);
                var girislog = new db.k_kullanici_giris_log
                {
                    Ip = GetIPAddress(),
                    islem_zaman = DateTime.Now,
                    kullanici_ad = KullaniciAd,
                    kullanici_id = kullanici.id
                };
                db.k_kullanici_giris_log.InsertOnSubmit(girislog);
                db.SubmitChanges();
                return ds;
            }

        }
        [WebMethod(EnableSession = true)]
        public DataSet MenuGetir()
        {
            if (Session["Kullanici"] == null)
            {
                DataTable dt = YanitOlustur.YanitSablon();
                DataSet ds = new DataSet("yanit");
                dt.Rows.Add("0", "yok");
                ds.Tables.Add(dt);
                return ds;
            }
            else
            {
                var menuler = db.k_menus.Where(a => a.aktif == 1);
                DataTable dt = new DataTable();
                dt.Columns.Add("yetki", typeof(string));
                dt.Columns.Add("sira", typeof(string));
                dt.Columns.Add("id", typeof(string));
                dt.Columns.Add("name", typeof(string));
                dt.Columns.Add("ust_menu", typeof(string));
                dt.Columns.Add("aktif", typeof(string));
                dt.Columns.Add("function", typeof(string));
                dt.Columns.Add("background", typeof(string));
                dt.Columns.Add("foreground", typeof(string));
                dt.Columns.Add("pic", typeof(string));
                dt.Columns.Add("col", typeof(string));
                dt.Columns.Add("row", typeof(string));
                dt.Columns.Add("sub_function", typeof(string));
                foreach (var item in menuler)
                {
                    dt.Rows.Add(item.yetki,item.sira,item.id,item.name, item.ust_menu, item.aktif, item.function, item.background, item.foreground, item.pic, item.col, item.row, item.sub_function);

                }
                DataTable dt2 = YanitOlustur.YanitSablon();
                DataSet ds2 = new DataSet("yanit");
                dt2.Rows.Add("1", "");
                ds2.Tables.Add(dt2);
                ds2.Tables.Add(dt);
                return ds2;
            }
        }
    }
}
