﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using yemekhane_web_servis.classes;

namespace yemekhane_web_servis
{
    /// <summary>
    /// Summary description for yemekhane
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class yemekhane : System.Web.Services.WebService
    {
        db.veritabaniDataContext db = new yemekhane_web_servis.db.veritabaniDataContext();

       
        [WebMethod(EnableSession = true)]
        public DataSet KampusGetir(string token, string KampusId)
        {
            Dogrulama dr = new Dogrulama(token, "y_yemekhane_gor");
            if (dr.Dogrulandi)
            {
                Genelislemler gi = new Genelislemler();
                var ka = (db.k_kullanici)Session["Kullanici"];
                var Kampusler = db.kampus.Where(a => a.aktif == 1);
                DataTable KampuslerDt = new DataTable();
                KampuslerDt.Columns.Add("id", typeof(int));
                KampuslerDt.Columns.Add("kampus_adi", typeof(string));
                foreach (var item in Kampusler)
                {
                    KampuslerDt.Rows.Add(item.id, item.kampus_adi);
                }
                DataTable dt = YanitOlustur.YanitSablon();
                dt.Rows.Add("1", gi.TokenEkle(ka.kullanici_adi));
                DataSet ds = new DataSet();
                ds.Tables.Add(dt);
                ds.Tables.Add(KampuslerDt);
                return ds;
            }
            else
            {
                DataTable dt = YanitOlustur.YanitSablon();
                dt.Rows.Add("0", "");
                DataSet ds = new DataSet();
                ds.Tables.Add(dt);
                return ds;
            }
        }
        [WebMethod(EnableSession = true)]
        public DataSet KampusEkle(string token, string KampusAdi)
        {
            if (Session["Kullanici"]==null)
            {
                DataTable dt4 = YanitOlustur.YanitSablon();
                dt4.Rows.Add("0", "yok");
                DataSet ds4 = new DataSet();
                ds4.Tables.Add(dt4);
                return ds4;
            }
           
            Dogrulama dr = new Dogrulama(token, "y_yemekhane_duzenle");
            if (dr.Dogrulandi)
            {
                Genelislemler gi = new Genelislemler();
                var ka = (db.k_kullanici)Session["Kullanici"];
                var KampusVarmi = db.kampus.Where(a => a.aktif == 1 && a.kampus_adi == KampusAdi).FirstOrDefault();
                if (KampusVarmi!=null)
                {
                    DataTable dt = YanitOlustur.YanitSablon();
                    dt.Rows.Add("kampusmevcut", gi.TokenEkle(ka.kullanici_adi));
                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    return ds;
                }
                var YeniKampus = new db.kampus
                {
                    aktif = 1,
                    kampus_adi = KampusAdi
                };
                db.kampus.InsertOnSubmit(YeniKampus);
                db.SubmitChanges();
                DataTable dt2 = YanitOlustur.YanitSablon();
                dt2.Rows.Add("1", gi.TokenEkle(ka.kullanici_adi));
                DataSet ds2 = new DataSet();
                ds2.Tables.Add(dt2);
                return ds2;

            }
            DataTable dt3 = YanitOlustur.YanitSablon();
            dt3.Rows.Add("0",dr.msg);
            DataSet ds3 = new DataSet();
            ds3.Tables.Add(dt3);
            return ds3;
        }
        [WebMethod(EnableSession = true)]
        public DataSet KampusDuzenleSil(string token, string KampusAdi,int KampusId,string islem)
        {
            Dogrulama dr = new Dogrulama(token, "y_yemekhane_duzenle");
            if (dr.Dogrulandi)
            {
                Genelislemler gi = new Genelislemler();
                var ka = (db.k_kullanici)Session["Kullanici"];
                var MevcutKampus = db.kampus.Where(a => a.aktif == 1 && a.id==KampusId).FirstOrDefault();
                if (MevcutKampus != null)
                {
                    if (islem=="duzenle")
                    {
                        var KampusVarmi = db.kampus.Where(a => a.aktif == 1 && a.kampus_adi == KampusAdi).FirstOrDefault();
                        if (KampusVarmi != null)
                        {
                           
                                DataTable dt = YanitOlustur.YanitSablon();
                                dt.Rows.Add("kampusmevcut", gi.TokenEkle(ka.kullanici_adi));
                                DataSet ds = new DataSet();
                                ds.Tables.Add(dt);
                                return ds;
                        }
                        else
                        {
                            MevcutKampus.islemyapankullanici = ka.kullanici_adi;
                            MevcutKampus.islemzamani = DateTime.Now;
                            db.kampus.InsertOnSubmit(MevcutKampus);
                            MevcutKampus.kampus_adi = KampusAdi;
                            db.SubmitChanges();
                            DataTable dt = YanitOlustur.YanitSablon();
                            dt.Rows.Add("1", gi.TokenEkle(ka.kullanici_adi));
                            DataSet ds = new DataSet();
                            ds.Tables.Add(dt);
                            return ds;

                        }
                    }
                    else if (islem=="sil")
                    {
                        MevcutKampus.aktif = 0;
                        MevcutKampus.islemyapankullanici = ka.kullanici_adi;
                        MevcutKampus.islemzamani = DateTime.Now;
                        db.SubmitChanges();
                    }
                    
                    
                   
                   
                }
               
               

            }
            DataTable dt3 = YanitOlustur.YanitSablon();
            dt3.Rows.Add("0", "");
            DataSet ds3 = new DataSet();
            ds3.Tables.Add(dt3);
            return ds3;
        }

    }
}
