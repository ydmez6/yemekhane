﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using yemekhane_web_servis.classes;
namespace yemekhane_web_servis
{
    /// <summary>
    /// Kullanici için özet açıklama
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Bu Web Hizmeti'nin, ASP.NET AJAX kullanılarak komut dosyasından çağrılmasına, aşağıdaki satırı açıklamadan kaldırmasına olanak vermek için.
    // [System.Web.Script.Services.ScriptService]
    public class Kullanici : System.Web.Services.WebService
    {
        db.veritabaniDataContext db = new yemekhane_web_servis.db.veritabaniDataContext();

        bool KullaniciDogrula()
        {
            if (Session["Kullanici"] != null)
                return true;

            return false;
        }
        [WebMethod(EnableSession = true)]
        public DataSet KullaniciEkleGuncelle(string k_adsoyad, string k_sifre, string k_ad, byte k_girisizni, int k_rol, string token, string islem)
        {
            Dogrulama dr = new Dogrulama(token, "k_kullanici_ekle");
            if (dr.Dogrulandi)
            {
                var ka = (db.k_kullanici)Session["kullanici"];
                Genelislemler gi = new Genelislemler();
                if (islem == "guncelle" || islem == "sil")
                {
                    var eskikullanici = db.k_kullanici.Where(a => a.aktif == 1 && a.kullanici_adi == k_ad).FirstOrDefault();
                    eskikullanici.aktif = 0;
                }
                if (islem != "sil")
                {
                    db.k_kullanici _kullanici = new db.k_kullanici
                    {
                        ad_soyad = k_adsoyad,
                        kullanici_adi = k_ad,
                        sifre = k_sifre,
                        giris_izni = k_girisizni,
                        aktif = 1,
                        rol_id = k_rol,
                        kayit_zaman = DateTime.Now
                    };
                    db.k_kullanici.InsertOnSubmit(_kullanici);
                }

                db.SubmitChanges();
                DataTable dt = YanitOlustur.YanitSablon();
                dt.Rows.Add("1", gi.TokenEkle(ka.kullanici_adi));
                DataSet ds = new DataSet();
                ds.Tables.Add(dt);
                return ds;
            }
            DataTable dt2 = YanitOlustur.YanitSablon();
            dt2.Rows.Add("0", "");
            DataSet ds2 = new DataSet();
            ds2.Tables.Add(dt2);
            return ds2;
        }
        [WebMethod(EnableSession = true)]
        public DataSet KullaniciListele(string KullaniciAdi, byte Aktif, string token)
        {
            Dogrulama dr = new Dogrulama(token, "k_kullanici_listele");
            if (dr.Dogrulandi)
            {
                var ka = (db.k_kullanici)Session["kullanici"];
                Genelislemler gi = new Genelislemler();

                var kullanicilar = db.k_kullanici.Where(a => (a.aktif == a.aktif || Aktif == 3) && (a.kullanici_adi == KullaniciAdi || KullaniciAdi == ""));
                DataTable dt = new DataTable();
                dt.Columns.Add("id", typeof(string));
                dt.Columns.Add("kullanici_adi", typeof(string));
                dt.Columns.Add("ad_soyad", typeof(string));
                dt.Columns.Add("sifre", typeof(string));
                dt.Columns.Add("rol_id", typeof(int));
                dt.Columns.Add("kayit_zaman", typeof(DateTime));
                dt.Columns.Add("aktif", typeof(byte));
                dt.Columns.Add("giris_izni", typeof(byte));
                dt.Columns.Add("son_giris_zaman", typeof(DateTime));
                foreach (var item in kullanicilar)
                {
                    dt.Rows.Add(item.id, item.kullanici_adi,item.ad_soyad,item.sifre,item.rol_id,item.kayit_zaman,item.aktif,item.giris_izni,item.son_giris_zaman);
                }
                DataTable dt2 = YanitOlustur.YanitSablon();
                dt.Rows.Add("1", gi.TokenEkle(ka.kullanici_adi));
                DataSet ds = new DataSet();
                ds.Tables.Add(dt2);
                ds.Tables.Add(dt);
                return ds;
            }
            
            DataTable dt3 = YanitOlustur.YanitSablon();
            dt3.Rows.Add("0", "");
            DataSet ds2 = new DataSet();
            ds2.Tables.Add(dt3);
            return ds2;
        }

    }
}
